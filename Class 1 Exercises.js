/**
 * MATH
 */

// 1. Pagliacci charges $16.99 for a 13” pizza and $19.99 for a 17” pizza.
// What is the area for each of these pizzas?
// (radius would be the listed size - i.e. 13" - divided by 2)
const thirteenArea = (Math.PI*((13/2)**2)).toFixed(2);
const seventeenArea = (Math.PI*((17/2)**2)).toFixed(2);
console.log(`13 inch pizza has an area of ${thirteenArea} inches`);
console.log(`17 inch pizza has an area of ${seventeenArea} inches`);


// 2. What is the cost per square inch of each pizza?
const thirteenCost = (16.99/thirteenArea);
const seventeenCost = (19.99/seventeenArea);
console.log(`13 inch pizza is $${thirteenCost.toFixed(2)} per inch`);
console.log(`17 inch pizza is $${seventeenCost.toFixed(2)} per inch`);


// 3. Using the Math object, put together a code snippet
// that allows you to draw a random card with a value
// between 1 and 13 (assume ace is 1, jack is 11…)
const firstCard = ((Math.random()*13)+1).toFixed(0);
const secondCard = ((Math.random()*13)+1).toFixed(0);
const thirdCard = ((Math.random()*13)+1).toFixed(0);


// 4. Draw 3 cards and use Math to determine the highest
// card
const highestcard = (Math.max(firstCard, secondCard, thirdCard));

console.log(`My cards are ${firstCard}, ${secondCard}, ${thirdCard}`);
console.log(`My higest card is ${highestcard}`);

/**
 * ADDRESS LINE
 */

// 1. Create variables for firstName, lastName,
// streetAddress, city, state, and zipCode. Use
// this information to create a formatted address block
// that could be printed onto an envelope.
const firstName = `Fletcher`;
const lastName = `Bonds`;
const streetAddress = `1234 Main St`;
const city = `Seattle`;
const state = `WA`;
const zipCode = `98119`;
const myAddress = (`${firstName} ${lastName}\n${streetAddress}\n${city}, ${state} ${zipCode}`);
console.log(myAddress);

// 2. You are given a string in this format:
// firstName lastName(assume no spaces in either)
// streetAddress
// city, state zip(could be spaces in city and state)
// 
// Write code that is able to extract the first name from this string into a variable.
// Hint: use indexOf, slice, and / or substring
const myFirstName = ((myAddress).substr(0,8));
console.log(myFirstName);


/**
 * FIND THE MIDDLE DATE
 */
// On your own find the middle date(and time) between the following two dates:
// 1/1/2020 00:00:00 and 4/1/2020 00:00:00
//
// Look online for documentation on Date objects.

// Starting hint:
const startDate = new Date(2020, 0, 1);
const endDate = new Date(2020, 3, 1);
const halfway = startDate.getTime() + ((endDate.getTime() - startDate.getTime())/2);
const middleDate = new Date(halfway);
console.log(`Start date: ${startDate}\nMiddle date: ${middleDate}\nEnd date: ${endDate}`);



